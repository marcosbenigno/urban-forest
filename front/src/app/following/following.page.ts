import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-following',
  templateUrl: './following.page.html',
  styleUrls: ['./following.page.scss'],
})
export class FollowingPage implements OnInit {

  userId: any = localStorage.getItem('user_id') ? localStorage.getItem('user_id') : localStorage.getItem('my_id');
  loggedUserId: any = localStorage.getItem('my_id');
  followings: any;
  userName: any;
  loggedUserFollowingList: any;
  followingsArray: any = [];
  

  constructor(public userService: UserService) { }

  ngOnInit() {
    this.getFollowingList();
    this.getFollowings();
   
    //console.log(this.followings.id);
    //console.log(this.loggedUserFollowingList);
  }

  getFollowings() {
    this.userService.listFollowers(this.userId ? this.userId : this.loggedUserId).subscribe((res) => {
      this.followings = res.followers;
      
      this.followings.forEach(element => {
        this.followingsArray.push(element.id)
        });
        //console.log(this.followingsArray);
    });
  }

  getFollowingList() {
    this.userService.listFollowers(this.userId ? this.userId : this.loggedUserId).subscribe((res) => {
      this.loggedUserFollowingList = res.followers;
      console.log(res);
    });
  }

  getUser() {
    this.userService.showUser(this.userId).subscribe((res) => {
      this.userName = res.user.name;
    });
  }

}
