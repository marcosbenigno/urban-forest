<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest as UserRequest;

use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasApiTokens;

    /*Função na Model para criar um Usuário*/
    public function createUser(UserRequest $request){
        $this->name = $request->name;
        $this->email = $request->email;
        $this->password = bcrypt($request->password);
        $this->profile_picture = $request->profile_picture;
        $this->identifier = $request->identifier;
        $this->nickname = $request->nickname;
        $this->bio = $request->bio;
        $this->notifications = $request->notifications;
        $this->save();
    }

    /*Função na Model para listar vários Usuários*/
    public function indexUser() {
    	$users = User::all();
    	return response()->json(['users' => $users],200);
	}

    /*Função na Model para mostrar um Usuário*/
	public function showUser($id) {
    	$user = User::find($id);
    	return response()->json(['user' => $user],200);
	}

    /*Função na Model para atualizar um Usuário*/
    public function updateUser(Request $request){
        if($request->name){
            $this->name = $request->name;
        }
        if($request->email){
            $this->email = $request->email;
        }
        if($request->password){
            $this->password = $request->password;
        }
        if($request->profile_picture){
            $this->profile_picture = $request->profile_picture;
        }
        if($request->identifier){
            $this->identifier = $request->identifier;
        }
        if($request->nickname){
            $this->nickname = $request->nickname;
        }
        if($request->bio){
            $this->bio = $request->bio;
        }
        if($request->notifications){
            $this->notifications = $request->notifications;
        }
        $this->save();
    }

    /*Abaixo, funções para a definição do relacionamento*/
    public function likes(){
        return $this->belongsToMany('App\Models\Post', 'likes');
    }

    public function followers(){
        return $this->belongsToMany('App\Models\User', 'follows', 'followers_id', 'following_id');
    }

    public function following(){
        return $this->belongsToMany('App\Models\User', 'follows', 'following_id', 'followers_id');
    }

    public function listLikes($id) {
       return DB::table('likes')->where('user_id', $id)->get();
    }
    public function listFollowers($id) {
        return DB::table('follows')->where('following_id', $id)->get('followers_id')->unique();
    }

    public function listFollowings($id) {
        return DB::table('follows')->where('followers_id', $id)->get('following_id');
    }



    //Post
    public function posts(){
        return $this->hasMany('App\Models\Post');
    }
    //Comment
    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
