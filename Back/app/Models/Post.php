<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Post extends Model
{
    use HasFactory;

    /*Função na Model para a criação de um Post */
    public function createPost(Request $request){
        $this->user_id = $request->user_id;
        $this->number_comments = $request->number_comments;
        $this->number_likes = $request->number_likes;
        $this->title = $request->title;
        $this->text = $request->text;
        $this->media = $request->media;
        $this->save();
    }

    /*Função na Model para listar vários Post */
    public function indexPost() {
    	$posts = Post::all();
    	return response()->json(['posts' => $posts],200);
	}

    /*Função na Model para mostrar um Post */
	public function showPost($id) {
    	$post = Post::find($id);
    	return response()->json(['post' => $post],200);
	}

    /*Função na Model para atualizar um Post */
    public function updatePost(Request $request){
        if($request->user_id){
            $this->user_id = $request->user_id;
        }
        if($request->number_comments){
            $this->number_comments = $request->number_comments;
        }
        if($request->number_likes){
            $this->number_likes = $request->number_likes;
        }
        if($request->title){
            $this->title = $request->title;
        }
        if($request->text){
            $this->text = $request->text;
        }
        if($request->media){
            $this->media = $request->media;
        }
        $this->save();
    }
    
    /*Função na Model para listar vários Post com base no Usuário desejado */
    public function listPost($user_id){
        return Post::where("user_id", "=" , $user_id)->get();
    }

    /*Abaixo, funções para a definição do relacionamento*/
    public function user(){
        return $this->belongsTo('App\Models\Post');
    }
    public function likesPost(){
        return $this->belongsToMany('App\Models\User', 'likes');
    }
    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }
}
    
