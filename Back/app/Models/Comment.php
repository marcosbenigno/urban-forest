<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Comment extends Model
{
    use HasFactory;

    /*Função na Model para criar um comentário*/ 
    public function createComment(Request $request){
        $this->post_id = $request->post_id;
        $this->user_id = $request->user_id;
        $this->number_likes = $request->number_likes;
        $this->number_comments = $request->number_comments;
        $this->text = $request->text;
        $this->save();
    }

    /*Função na Model para listar vários comentários*/ 
    public function indexComment() {
    	$comments = Comment::all();
    	return response()->json(['comments' => $comments],200);
	}

    /*Função na Model para mostrar um comentário*/ 
	public function showComment($id) {
    	$comment = Comment::find($id);
    	return response()->json(['comment' => $comment],200);
	}

    /*Função na Model para atualizar um comentário*/ 
    public function updateComment(Request $request){
        if($request->post_id){
            $this->post_id = $request->post_id;
        }
        if($request->user_id){
            $this->user_id = $request->user_id;
        }
        if($request->number_comments){
            $this->number_comments = $request->number_comments;
        }
        if($request->number_likes){
            $this->number_likes = $request->number_likes;
        }
        if($request->text){
            $this->text = $request->text;
        }
    }

    /*Função na Model para mostrar os comentários associados a um post*/ 
    public function listComment($post_id){
        return Comment::where("post_id", "=" , $post_id)->get();
    }

    /*Abaixo, funções para a definição do relacionamento*/
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function post(){
        return $this->belongsTo('App\Models\Post');
    }
}
