<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PassportRequest as PassportRequest;
use App\Models\User;
use Auth;
use DB;

class PassportController extends Controller
{
    /*Função na Passport para o Registro de um novo usuário*/
    public function register (Request $request){
    	$user = new User;
    	$user->createUser($request);
    	$user->save();
    	return response()->json([
        	"message" => "Usuário registrado.",
        	"user" => $user
    	], 200);
    }

    /*Função na Passport para o Login de um novo usuário*/
    public function login() {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
          $user = Auth::user();
          $token = $user->createToken('UrbanForest')->accessToken;
          return response()->json(["message" => "Login concluido!", "data" => ["user" => $user,"token" => $token]], 200);
        }
        else {
          return response()->json(["message" => "Email ou senha inválidos!", "data" => [null]], 500);
        }
    }

    public function getDetails (){
    	$user = Auth::user();
    	return response()->json(["user" => $user], 200);
    }

    /*Função na Passport para o Logouts de um novo usuário*/
    public function logout (){
    	$accessToken = Auth::user()->token();
    	DB::table('oauth_refresh_tokens')->where('access_token_id', $accessToken->id)->update(['revoked' => true]);
    	$accessToken->revoke();
    	return response()->json(["Usuário deslogado."], 200);
    }
}
