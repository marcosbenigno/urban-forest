<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest as PostRequest;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /*Função na Controller para criar um Post*/
    public function createPost(Request $request){
        $post = new Post;
        $post->createPost($request);
        return response()->json(['message' => 'post criado', 'post' => $post], 200);
    }

    /*Função na Controller para listar vários Post*/
    public function indexPost(){
        $posts = Post::all();
        return response()->json(['post' => $posts], 200);
    }

    /*Função na Controller para mostrar um Post*/
    public function showPost($id){
        $post = Post::find($id);
        return response()->json(['post' => $post], 200);
    }

    /*Função na Controller para atualizar um Post*/
    public function updatePost(Request $request, $id){
        $post = Post::find($id);
        $post->updatePost($request);
        return response()->json(['message' => 'post criado', 'post' => $post], 200);
    }

    /*Função na Controller para deletar um Post*/
    public function destroyPost($id){
        $post = Post::find($id);
        $post->destroy($id);
        return response()->json(['Post deletado com sucesso!' => $post], 200);
    }

    /*Função na Controller para associar um Post a um Usuário*/
    public function addUser($id, $user_id){
        $post = Post::findOrFail($id);
        $user = User::findOrFail($user_id);
        $post->user_id = $user_id;
        $post->save();
        return response()->json($post);
    }

    /*Função na Controller para desassociar um Post a um Usuário*/
    public function removeUser($id, $user_id){
        $post = Post::findOrFail($id);
        $user = User::findOrFail($user_id);
        $post->user_id = NULL;
        $post->save();
        return response()->json($post);
    }

    /*Função na Controller para listar vários Post com base no Usuário*/
    public function listPost($id){
        $list = new Post;
        return response()->json(['list' => $list->listPost($id)]);
    } 

    /*Função na Controller para listar os Post dos usuários que eu sigo*/
    public function listPostFollow (){
        $user = Auth::user();
        $followings = $user->following;
        $response = array();

        foreach($followings as $following){
            $post = $following->posts;
            array_push($response, $post);
        }
        return response()->json(['post' => $response]);
    }
}
