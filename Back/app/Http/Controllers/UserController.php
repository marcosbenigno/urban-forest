<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest as UserRequest;
use App\Models\User;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    /*Função na Controller para criar um Usuário*/
    public function createUser(UserRequest $request){
        $user = new User;
        $user->createUser($request);
        return response()->json(['message' => 'Usuário criado', 'user' => new UserResource($user)], 200);
    }

    /*Função na Controller para listar vários Usuário*/
    public function indexUser(){
        $users = User::all();
        return response()->json(['user' => $users], 200);
    }

    /*Função na Controller para mostrar um Usuário*/
    public function showUser($id){
        $user = User::find($id);
        return response()->json(['user' => $user], 200);
    }

    /*Função na Controller para atualizar os dados de um Usuário*/
    public function updateUser(Request $request, $id){
        $user = User::find($id);
        $user->updateUser($request);
        return response()->json(['message' => 'User criado', 'user' => $user], 200);
    }

    /*Função na Controller para destruir um Usuário*/
    public function destroyUser($id){
        $user = User::find($id);
        $user->destroy($id);
        return response()->json(['Usuário deletado com sucesso!' => $user], 200);
    }

    /*Função na Controller para seguir um Usuário*/
    public function follow ($id){
        $user = User::find($id);
        $on_user = Auth::user();
        $on_user->followers()->attach($id);
        $user->following()->attach($on_user->id);
        return response()->json(['mensagem' => 'seguiu'], 200);
    }

    /*Função na Controller para deixar de seguir um Usuário*/
    public function unfollow ($id){
        $user = User::find($id);
        $on_user = Auth::user();
        $on_user->followers()->detach($id);
        $user->following()->detach($on_user->id);
        return response()->json(['mensagem' => 'deixou de seguir'], 200);
    }

    /*Função para dar Like em um Post de um Usuário*/
    public function likes($id){
        $on_user = Auth::user();
        $on_user->likes()->attach($id);
        return response()->json(['mensagem' => 'curtiu'], 200);
    }

    /*Função para dar Deslike em um Post de um Usuário*/
    public function deslike($id){
        $on_user = Auth::user();
        $on_user->likes()->detach($id);
        return response()->json(['mensagem' => 'deixou de curtir'], 200);
    }

    /*Função para listar os Likes de um Usuário*/
    public function listLikes($id){
        $user = new User;
        return response()->json(['likes' => $user->listLikes($id)], 200);
    }

    /*Função para listar os seguidores de um Usuário*/
    public function listFollowers($id){
        $user = new User;
        return response()->json(['followers' => $user->listFollowers($id)], 200);
    }

    /*Função para listar quem segue um Usuário*/
    public function listFollowings($id){
        $user = new User;
        return response()->json(['followings' => $user->listFollowings($id)], 200);
    }
}
