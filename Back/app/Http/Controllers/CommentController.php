<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest as CommentRequest;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /*Função na Controller para criar um comentário*/
    public function createComment(Request $request){
        $comment = new Comment;
        $comment->createComment($request);
        return response()->json(['message' => 'Comentários criado', 'comment' => $comment], 200);
    }

    /*Função na Controller para listar vários comentários*/
    public function indexComment(){
        $comments = Comment::all();
        return response()->json(['comment' => $comments], 200);
    }

    /*Função na Controller para listar um comentários*/
    public function showComment($id){
        $comment = Comment::find($id);
        return response()->json(['comment' => $comment], 200);
    }

    /*Função na Controller para atualizar um comentário*/
    public function updateComment(Request $request, $id){
        $comment = Comment::find($id);
        $comment->updateComment($request);
        return response()->json(['message' => 'Comentário atualizado', 'comment' => $comment], 200);
    }

    /*Função na Controller para destruir um comentário*/
    public function destroyComment($id){
        $comment = Comment::find($id);
        $comment->destroy($id);
        return response()->json(['Comentário deletado com sucesso!' => $comment], 200);
    }

    /*Função na Controller para listar vários comentários em um post*/
    public function listComment($id){
        $list = new Comment;
        return response()->json(['comment' =>$list->listComment($id)]);
    }  
}
