<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     /*Função para checar se um Usuário é SuperAdmin*/
    public function handle(Request $request, Closure $next)
    {
        $on_user = Auth::user();
        $user = User::findOrFail($request->id);
        if ($on_user->identifier == 1 || $request->id == $on_user->id){
            return $next($request);
        }
        else{
            return response()->json(['Impossível realizar essa ação']);
        }
    }
}
