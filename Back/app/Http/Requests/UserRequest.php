<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:Users,email',
            'password' => 'required|string',
            'number_followers' => 'integer', 
            'number_followings' => 'integer',
            'identifier' => 'integer',  
            'nickname' => 'required|string',
            'bio' => 'string', 
            'notifications' => 'string', 
        ];
    }

    public function messages (){
        return [
            'name.required' => 'O nome não pode ser nulo',
            'email.required' => 'O email não pode ser nulo',
            'email.email' => 'O email está no formato errado',
            'email.unique' => 'Esse email já existe na Base de Dados',
            'password.required' => 'A password não pode ser nulo',
            'nickname.required' => 'O nickname não pode ser nulo',
        ];
    }

    protected function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json($validator->errors(),422));
    }
}
