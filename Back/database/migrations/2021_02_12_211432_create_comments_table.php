<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Método de Criação da Tabela Comments/Comentários */
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->string('text');
            $table->string('number_comments')->nullable();
            $table->string('number_likes')->nullable();
            $table->timestamps();
        });

        Schema::table('comments', function (Blueprint $table){
            $table->foreign('post_id')->references('id')->on("posts")->onDelete('cascade');
        });
        Schema::table('comments', function (Blueprint $table){
            $table->foreign('user_id')->references('id')->on("users")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
