<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->integer('number_followers')->nullable();
            $table->integer('number_following')->nullable();
            $table->integer('identifier');
            $table->string('nickname');
            $table->string('bio')->nullable();
            $table->string('notifications')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table){  
            DB::statement("ALTER TABLE users ADD profile_picture MEDIUMBLOB");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
