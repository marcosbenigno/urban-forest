<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;

/*Abaixo, trecho de código referente à autenticação*/
use App\Http\Controllers\PassportController;

use App\Models\User;

/*use App\Http\Controllers\API\PassportController;*/

Route::post('register', [PassportController::class, 'register']);
Route::post('login', [PassportController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function() {
	// Passport Controller
	Route::get('logout', [PassportController::class, 'logout']);
	Route::get('getDetails', [PassportController::class, 'getDetails']);

    /*Rotas para as funções de relacionamento*/
    Route::get('follow/{id}', [UserController::class, 'follow']);
    Route::get('unfollow/{id}', [UserController::class, 'unfollow']);
    Route::get('likes/{id}', [UserController::class, 'likes']);
    Route::get('deslike/{id}', [UserController::class, 'deslike']);

    /*Rota da função para listar todos os posts de quem o usuário segue*/
    Route::get('listPostFollow', [PostController::class, 'listPostFollow']);

    /*Rotas protegidas relacionadas a User */
    Route::patch('updateUser/{id}', [UserController::class, 'updateUser'])->middleware('check');
    Route::delete('destroyUser/{id}', [UserController::class, 'destroyUser'])->middleware('check');

    /*Rotas protegidas relacionadas a Post */
    Route::post('createPost', [PostController::class, 'createPost']);
    Route::patch('updatePost/{id}', [PostController::class, 'updatePost'])->middleware('check');
    Route::delete('destroyPost/{id}', [PostController::class, 'destroyPost'])->middleware('check');

    /*Rotas protegidas relacionadas a Comment */
    Route::post('createComment', [CommentController::class, 'createComment']);
    Route::patch('updateComment/{id}', [CommentController::class, 'updateComment'])->middleware('check');
    Route::delete('destroyComment/{id}', [CommentController::class, 'destroyComment'])->middleware('check');

});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Routes referentes a página UserController com suas devidas funcionalidades*/
Route::get('listLikes/{id}', [UserController::class, 'listLikes']);
Route::get('listFollowers/{id}', [UserController::class, 'listFollowers']);
Route::get('listFollowings/{id}', [UserController::class, 'listFollowings']);
Route::get('indexUser', [UserController::class, 'indexUser']);
Route::get('showUser/{id}', [UserController::class, 'showUser']);
Route::post('createUser', [UserController::class, 'createUser']);

/*Routes referentes a página PostController com suas devidas funcionalidades*/
Route::get('getPostByUser/{id}', [PostController::class, 'listPost']);
Route::get('indexPost', [PostController::class, 'indexPost']);
Route::get('showPost/{id}', [PostController::class, 'showPost']);


/*Routes referentes a página CommentController com suas devidas funcionalidades*/
Route::get('getPostByComment/{id}', [CommentController::class, 'listComment']);
Route::get('indexComment', [CommentController::class, 'indexComment']);
Route::get('showComment/{id}', [CommentController::class, 'showComment']);
